#include "iot_device.h"

IotDevice::IotDevice(PubSubClient* mqtt_client) 
	: mqtt(mqtt_client) 
{
	receiver = new MqttDataReceiver();
	Serial.println("IotDevice constructed");
}

void IotDevice::setup() {
	Serial.println("IotDevice.setup()");
	ready = false;
	addDhtSensor(3, DhtSensor::DHT_22, 3000, 2, 0.1);
	
	CoolSubject* cs = new CoolSubject(22, 23);
	for(int i=0; i<=last_dht_sensor; i++)
	    dht_sensors[i]->getTemperatureSubject()->subscribe(cs);

	MqttTopicFilter* wtf = new MqttTopicFilter("weather/temperature");
	MqttTopicFilter* whf = new MqttTopicFilter("weather/humidity");
	getReceiver()->subscribe(wtf);
	getReceiver()->subscribe(whf);
	
	FansSubject* fs = new FansSubject(10, 40);
	wtf->subscribe(fs);
	whf->subscribe(fs);

	Actuator* a = new Actuator(4, 6, 5, cs, fs);	
	ready = true;
}

void IotDevice::loop() {
	if(ready) {
		for(char i=0; i<=last_dht_sensor; i++) {
			dht_sensors[i]->loop();
		}
	}
}

void IotDevice::addDhtSensor(
		byte pin, 
		DhtSensor::Model model, 
		unsigned long interval, 
		float humidity_tolerance, 
		float temperature_tolerance) 
{
	Serial.println("IotDevice.addDhtSensor()");
	if(last_dht_sensor < DHT_MAX_SENSORS) {
		Serial.println("IotDevice.addDhtSensor() if");
		DhtSensor *sensor = new DhtSensor(pin, model, interval, humidity_tolerance, temperature_tolerance);
		dht_sensors[++last_dht_sensor] = sensor;
		Serial.println("IotDevice.addDhtSensor() instance add");
		char *topic = new char[15];
		snprintf(topic, 15, "dht/%d/humidity", last_dht_sensor);
		sensor->getHumiditySubject()
			->subscribe(new MqttHumiditySender(mqtt, topic));
		
		topic = new char[18];
		snprintf(topic, 18, "dht/%d/temperature", last_dht_sensor);
		Serial.println(topic);
		sensor->getTemperatureSubject()
			->subscribe(new MqttTemperatureSender(mqtt, topic));
	}
	Serial.println("IotDevice.addDhtSensor() finish");
}

DhtSensor* IotDevice::getDhtSensor(char index)  {
	return dht_sensors[index];
}
