#include "temperature_subject.h"

float TemperatureSubject::extractValue() const {
//	Serial.println("TemperatureSubject.extractValue()");
	return getSensor()->getTemperature();
}
