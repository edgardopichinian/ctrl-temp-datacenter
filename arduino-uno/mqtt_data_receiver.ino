#include "mqtt_data_receiver.h"

void MqttDataReceiver::receive(char* topic, byte* payload, unsigned int length) {
	Serial.print("MqttDataReceiver.receive(). Topic: ");
	Serial.print(topic);
	this->topic = topic;
	payload[length] = '\0';
	this->payload = String((char*) payload);
	Serial.print(", Payload: ");
	Serial.println(this->payload);
	this->length = length;
	notifyObservers(this);
}
