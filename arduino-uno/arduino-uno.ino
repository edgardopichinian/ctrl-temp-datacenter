#include <SPI.h>
#include <Ethernet.h>
#include <PubSubClient.h>

#include "iot_device.h"

#define CLIENT_ID "ArduinoUno"

EthernetClient eth_client;
PubSubClient mqtt_client;

IotDevice device(&mqtt_client);

void reconnect();

void callback(char* topic, byte* payload, unsigned int length);

void setup() {
	Serial.begin(9600);
	while(!Serial) {;}

	mqtt_client.setClient(eth_client);
//	Address mqtt_server_ip(170, 210, 221, 45);
	IPAddress mqtt_server_ip(192, 168, 1, 33);
	mqtt_client.setServer(mqtt_server_ip, 1883);
	mqtt_client.setCallback(callback);

	byte mac[] = {0xDE, 0xED, 0xBB, 0xFE, 0xFE, 0xED};
	// IPAddress ip(192, 168, 1, 100);
	IPAddress ip(192, 168, 1, 100);
	Ethernet.begin(mac, ip);
	delay(2000);

	device.setup();
}

void loop() {
	reconnect();
	mqtt_client.loop();
	device.loop();
	
}

void reconnect() {
	while (!mqtt_client.connected()) {
		Serial.print("Attempting MQTT connection...");
		if (mqtt_client.connect(CLIENT_ID)) {
			mqtt_client.subscribe("#");
			Serial.println("connected");
		} else {
			Serial.print("failed, rc=");
			Serial.print(mqtt_client.state());
			Serial.println(" try again in 5 seconds");
			delay(5000);
		}
	}
}

void callback(char* topic, byte* payload, unsigned int length) {
	device.getReceiver()->receive(topic, payload, length);
}
