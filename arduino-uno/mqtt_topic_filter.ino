#include "mqtt_topic_filter.h"

bool MqttTopicFilter::pass(const Subject* subject) const {
	char* t = ((MqttDataReceiver*)subject)->getTopic();
//	Serial.println("MqttTopicFilter.pass()");
//	Serial.print("topic received: ");
//	Serial.println(t);
//	Serial.print("topic filter: ");
//	Serial.println(topic);
	bool equals = strcmp(topic, t) == 0;
//	Serial.print("equals: ");
//	Serial.println(equals);
	return equals;
}

