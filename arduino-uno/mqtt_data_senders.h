#ifndef _MQTT_DATA_SENDERS_H
#define _MQTT_DATA_SENDERS_H

#include <PubSubClient.h>
// #include <ArduinoJson.h>
#include "observer.h"
#include "dht_sensor.h"

class MqttDataSender : public Observer {
	public:
		MqttDataSender(PubSubClient* client, const char* topic)
			: client(client), topic(topic) {}
		void notify(const Subject *sub); 

	protected:
		// virtual JsonObject extractMessage(const Subject* sub) = 0;
		virtual char* extractMessage(const Subject* sub) = 0;
		
	private:
		PubSubClient* client;
		const char* topic;
};

class MqttTemperatureSender : public MqttDataSender {
	public: 
		MqttTemperatureSender(PubSubClient* client, const char* topic)
			: MqttDataSender(client, topic) {
				Serial.println("MqttTemperatureSender created");
			}
	
	protected:
		char* extractMessage(const Subject* sub);
};

class MqttHumiditySender : public MqttDataSender {

	public: 
		MqttHumiditySender(PubSubClient* client, const char* topic)
			: MqttDataSender(client, topic) {
				Serial.println("MqttHumiditySender");
			}
	protected:
		char* extractMessage(const Subject* sub);
};

#endif
