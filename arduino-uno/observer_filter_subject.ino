#include "observer_filter_subject.h"

void ObserverFilterSubject::notify(const Subject* subject) {
//	Serial.println("ObserverFilterSubject.notify()");
	if(pass(subject)) {
		update(subject);
		notifyObservers(subject);
	}
}

