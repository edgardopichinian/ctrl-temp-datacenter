#include "dht_sensor.h"

DhtSensor::DhtSensor(
		byte p, 
		enum Model m, 
		unsigned long i = 5000L, 
		float ht = 0.1, 
		float tt = 0.1) 
	:
		pin(p), 
		model(m),
		humidity_subject(this, ht),
		temperature_subject(this, tt)
{	
	setInterval(i);
	pinMode(p, INPUT);
	Serial.print("Sensor created at pin ");
	Serial.println(pin);
}

void DhtSensor::read() {
//	Serial.println("DhtSensor.read()");
	int chk;
	switch (model)
	{
		case Model::DHT_11:
			chk = DHT.read11(pin);
			break;
		case Model::DHT_12:
			chk = DHT.read12(pin);
			break;
		case Model::DHT_21:
			chk = DHT.read21(pin);
			break;
		case Model::DHT_22:
			chk = DHT.read22(pin);
			break;
		case Model::DHT_2301:
			chk = DHT.read2301(pin);
			break;
		case Model::DHT_2302:
			chk = DHT.read2302(pin);
			break;
		case Model::DHT_2303:
			chk = DHT.read2303(pin);
			break;
		case Model::DHT_2320:
			chk = DHT.read2320(pin);
		case Model::DHT_2322:
			chk = DHT.read2322(pin);
			break;
		case Model::DHT_33:
			chk = DHT.read33(pin);
			break;
		case Model::DHT_44:
			chk = DHT.read44(pin);
			break;
		default:
			chk = DHT.read(pin);
	}
	if(chk != DHTLIB_OK) {
		Serial.println("dht error reading");
	}else {
//		Serial.print("temperature ");
//		Serial.println(DHT.temperature);
//		Serial.print("humidity ");
//		Serial.println(DHT.humidity);
		notifyObservers(this);
	}
}

void DhtSensor::setInterval(int interval) { 
	this->interval = interval < MIN_INTERVAL ? MIN_INTERVAL
		: interval > MAX_INTERVAL ? MAX_INTERVAL 
		: interval;
	next_reading = millis() + interval;
}

void DhtSensor::loop() {
	const unsigned long t = millis();
	if(next_reading <= t) {
		read();
		next_reading = t + interval;
	}
}
