#include "tolerance_filter.h"

ToleranceFilter::ToleranceFilter(DhtSensor* sensor, float tolerance) 
	: tolerance(tolerance) {
	this->sensor = sensor;
	sensor->subscribe(this);
}


bool ToleranceFilter::pass(const Subject* subject) const {
//  Serial.println("ToleranceFilter.pass()");
	return value == NULL || abs(*value - extractValue()) >= tolerance;
	return true;
}

void ToleranceFilter::update(const Subject* subject) {
//	Serial.println("ToleranceFilter.update()");
	if(value == NULL) value = new float;
	*value = extractValue();
}

void ToleranceFilter::setTolerance(float tolerance) {
//	Serial.println("ToleranceFilter.setTolerance()");
	this->tolerance = tolerance < MIN_TOLERANCE ? MIN_TOLERANCE :
		tolerance > MAX_TOLERANCE ? MAX_TOLERANCE : tolerance; 
}
