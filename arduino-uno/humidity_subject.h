#ifndef _HUMIDITY_SUBJECT_H
#define _HUMIDITY_SUBJECT_H

#include "tolerance_filter.h"

class HumiditySubject : public ToleranceFilter {
	public:
		HumiditySubject(DhtSensor* s, float t) : ToleranceFilter(s, t) {
			Serial.println("HumiditySubject created");
		}
	protected:
		float extractValue() const;
};

#endif
