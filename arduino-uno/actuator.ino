#include "actuator.h"

void CoolSubject::notify(const Subject* subject) {
	Serial.println("CoolSubject.notify()");
	if(DhtSensor* sensor = (DhtSensor*)subject) {
		if(!cool && sensor->getTemperature() >= temperature_max) {
			cool = true;
			notifyObservers(this);
		}else if(cool && sensor->getTemperature() <= temperature_min) {
			cool = false;
			notifyObservers(this);
		}
	}
}

void FansSubject::notify(const Subject* subject) {
	Serial.println("FansSubject.notify()");
	if(MqttDataReceiver* receiver = (MqttDataReceiver*) subject) {
		// weather/temperature
		char* topic = receiver->getTopic();
		float value = receiver->getPayload().toFloat();
		if(strcmp(topic, "weather/temperature") == 0) {
			if(!ext_tem) ext_tem = new float;
			*ext_tem = value;
			Serial.println("weather temperature receiverd");
		}else if(strcmp(topic, "weather/humidity") == 0) {
			if(!ext_hum) ext_hum = new float;
			*ext_hum = value;
			Serial.println("weather humidity receiverd");
		}		
	}
	if(!fans_available && ext_tem && ext_hum 
			&& *ext_tem <= ext_tem_max 
			&& *ext_hum <= humidity_max) {
		fans_available = true;
		notifyObservers(this);
	}else if(fans_available) {
		fans_available = false;
		notifyObservers(this);
	}
}

ActuatorState Actuator::resting(false, false, false);
ActuatorState Actuator::fanning(true, false, false);
ActuatorState Actuator::ac(false, false, true);

Actuator::Actuator(
		byte fans_power_pin, 
		byte fans_conmutator_pin, 
		byte acs_power_pin,
		CoolSubject* coolSubject,
		FansSubject* fansSubject)
	: 
		fans_power_pin(fans_power_pin), 
		fans_conmutator_pin(fans_conmutator_pin),
		acs_power_pin(acs_power_pin),
		coolSubject(coolSubject),
		fansSubject(fansSubject)	
{
	pinMode(fans_power_pin, OUTPUT);
	pinMode(fans_conmutator_pin, OUTPUT);
	pinMode(acs_power_pin, OUTPUT);
	coolSubject->subscribe(this);
	fansSubject->subscribe(this);
}

void Actuator::notify(const Subject* subject) {
	Serial.println("Actuator.notify()");
	if(coolSubject == subject) {
		cool = coolSubject->getCool();
		Serial.print("cool: "); Serial.println(cool);
	}else if(fansSubject == subject) {
		fans_available = fansSubject->getFansAvailable();
		Serial.print("fans_available: "); Serial.println(fans_available);
	}


	if(state != &fanning && cool && fans_available) {
		updateState(fanning);	
		Serial.println("Actuator.notify() Enfriando con ventiladores");
	}else if(state != &ac && cool && !fans_available) {
		updateState(ac);	
		Serial.println("Actuator.notify() Enfriendo con AC");
	}else if(state != &resting && !cool) {
		Serial.println("Actuator.notify() En reposo");
		updateState(resting);
	}
}

void Actuator::updateState(ActuatorState newState) {
	Serial.println("Actuator.updateState()");
	state = &newState;
	digitalWrite(acs_power_pin, state->getAcsPower() ? HIGH : LOW);
	digitalWrite(fans_conmutator_pin, state->getFansConmutator() ? HIGH : LOW);
	digitalWrite(fans_power_pin, state->getFansPower() ? HIGH : LOW);
}

