#ifndef _MQTT_TOPIC_FILTER
#define _MQTT_TOPIC_FILTER

#include <string.h>
#include "observer_filter_subject.h"

class MqttTopicFilter : public ObserverFilterSubject {

	public:
		MqttTopicFilter(char topic[]): topic(topic) {}

	protected:	
		bool pass(const Subject* subject) const;
		void update(const Subject* subject) {}

	private:
		char* topic;
};

#endif
