#ifndef _OBSERVER_FILTER_SUBJECT_H
#define _OBSERVER_FILTER_SUBJECT_H

#include "subject.h"

class ObserverFilterSubject : public Observer, public Subject {
	public:
		void notify(const Subject* subject);
	protected:
		virtual bool pass(const Subject* subject) const = 0;
		virtual void update(const Subject* subject) = 0;
};


#endif
