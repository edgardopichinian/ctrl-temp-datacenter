#ifndef _ACTUATOR_H
#define _ACTUATOR_H

#include "observer.h"

class CoolSubject : public Observer, public Subject {
	public:
		
		CoolSubject(float temperature_max, float temperate_min)
			:temperature_max(temperature_max), temperature_min(temperature_min) {}
		
		void notify(const Subject* sub);
		
		bool getCool() { return cool; }

	private:
		float temperature_max;
		float temperature_min;
		bool cool = false;
};

class FansSubject : public Observer, public Subject {
	public:
		
		FansSubject(float ext_tem_max,	float humidity_max)
			: ext_tem_max(ext_tem_max), humidity_max(humidity_max) {}
		
		void notify(const Subject* subject);
		
		bool getFansAvailable() { return fans_available; }

	private:
		float ext_tem_max;
		float humidity_max;
		// state
		float* ext_tem;
		float* ext_hum;
		bool fans_available = false;
};


class ActuatorState {
	public:
		ActuatorState(bool fans_power, bool fans_conmutator, bool acs_power)
			: fans_power(fans_power), fans_conmutator(fans_conmutator), acs_power(acs_power) {}
		bool getFansPower() { return fans_power; }
		bool getFansConmutator() { return fans_conmutator; }
		bool getAcsPower() { return acs_power; }	
	private:
		const bool fans_power;
		const bool fans_conmutator;
		const bool acs_power;
};

class Actuator : public Observer {
	
	static ActuatorState resting;
	static ActuatorState fanning;
	static ActuatorState ac;

	public:
		Actuator(
				byte fans_power_pin, 
				byte fans_conmutator_pin, 
				byte acs_power_pin,
				CoolSubject* coolSubject,
				FansSubject* fansSubject);
		
		void notify(const Subject* sub);
		
	private:
		byte fans_power_pin;
		byte fans_conmutator_pin;
		byte acs_power_pin;
		CoolSubject* coolSubject;
		FansSubject* fansSubject;
		
		ActuatorState* state = &resting;
		
		bool cool = false;
		bool fans_available = false;

		void updateState(ActuatorState newState);
};

#endif
