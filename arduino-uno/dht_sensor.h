#ifndef _DHT_SENSOR_H
#define _DHT_SENSOR_H

#define MAX_INTERVAL 60000L
#define MIN_INTERVAL 2000L

#include <dht.h>
#include "subject.h"
#include "humidity_subject.h"
#include "temperature_subject.h"

class DhtSensor : public Subject {
	public:
		enum Model {
			DHT_11,
			DHT_12,
			DHT_21,
			DHT_22,
			DHT_33,
			DHT_44,
			DHT_2301,
			DHT_2302,
			DHT_2303,
			DHT_2320,
			DHT_2322
		};
		

		DhtSensor(
				byte pin, 
				enum Model model, 
				unsigned long interval, 
				float humidity_tolerance, 
				float temperature_tolerance);
		
		int getInterval() const { return interval; }
		void setInterval(int interval);

		float getHumidity() const { return DHT.humidity; }
		float getTemperature() const { return DHT.temperature; }
		unsigned long getNextReading() const { return next_reading; }
		const HumiditySubject* getHumiditySubject() const { 
			return &humidity_subject;
		}
		const TemperatureSubject* getTemperatureSubject() const { 
			return &temperature_subject;
		}	
		void setup();
		void loop();
	private:
		// struct
		byte pin;
		const enum Model model;
		int interval;
		const HumiditySubject humidity_subject;
		const TemperatureSubject temperature_subject;
		// state
		unsigned long next_reading;
		dht DHT;

		void read();
};

#endif
