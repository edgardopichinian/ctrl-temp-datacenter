#include "mqtt_data_senders.h"

void MqttDataSender::notify(const Subject *sub) {
//	Serial.print("MqttDataSender.notify() ");
	char* message = extractMessage(sub);
	Serial.println(message);
	client->publish(topic, message, true);
	delete message;
}

char* MqttTemperatureSender::extractMessage(const Subject* sub) {
//	Serial.println("MqttTemperatureSender.extractMessage()");
	float t = ((DhtSensor*) sub)->getTemperature();
	char* message = new char[8];
	dtostrf(t, 6, 2, message);
	return message;
}

char* MqttHumiditySender::extractMessage(const Subject* sub) {
//	Serial.println("MqttHumiditySender.extractMessage()");
	// DhtSensor* sensor = (DhtSensor*) sub;
	// StaticJsonDocument<JSON_OBJECT_SIZE(2)> doc;
	// JsonObject object = doc.to<Message>();
	// object["time"] = sensor->getLastReading();
	// object["humidity"] = sensor->getTemperature();
	float t = ((DhtSensor*) sub)->getHumidity();
	char* message = new char[8];
	dtostrf(t, 6, 2, message);
	return message;
}

