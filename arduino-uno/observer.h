#ifndef _OBSERVER_H
#define _OBSERVER_H

#include<Arduino.h>

class Subject;

class Observer {
	public:
		virtual void notify(const Subject* sub) = 0;
};

#endif
