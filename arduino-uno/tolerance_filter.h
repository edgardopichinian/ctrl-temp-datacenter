#ifndef _TOLERANCE_FILTER_H
#define _TOLERANCE_FILTER_H

#define MAX_TOLERANCE 1
#define MIN_TOLERANCE 0.005

#include "observer_filter_subject.h"

class DhtSensor;

class ToleranceFilter : public ObserverFilterSubject {
	public:
		ToleranceFilter(DhtSensor* sensor, float tolerance);
		float getTolerance() const { return tolerance; }
		void setTolerance(float tolerance);
		float getValue() const { return *value; }
		const DhtSensor* getSensor() const { return sensor;}

	protected:
		bool pass(const Subject* subject) const;
		void update(const Subject* subject);
		virtual float extractValue() const = 0;
	
	private:
		DhtSensor* sensor;
		float tolerance;
		float* value = NULL;
};

#endif
