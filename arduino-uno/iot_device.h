#ifndef _IOT_DEVICE_H
#define _IOT_DEVICE_H


#include <PubSubClient.h>
#include "dht_sensor.h"
#include "mqtt_data_receiver.h"
#include "mqtt_data_senders.h"
#include "mqtt_topic_filter.h"

#define DHT_MAX_SENSORS 2

void(* resetFunc) (void) = 0;

class IotDevice {

	public:
		IotDevice(PubSubClient* mqtt_client);

		void addDhtSensor(
				byte pin,
				DhtSensor::Model model,
				unsigned long interval,
				float humidity_tolerance,
				float temperature_tolerance);

		DhtSensor* getDhtSensor(char index);

		void setup();
		void loop();
		MqttDataReceiver* getReceiver() const { return receiver; };

	private:
		PubSubClient* mqtt;
		MqttDataReceiver* receiver;
		boolean ready = false;
		char last_dht_sensor = -1;
		DhtSensor *dht_sensors[DHT_MAX_SENSORS];
};
/*
struct SensorConfig {
   bool enabled;
   byte pin;
   DhtSensor::Model model;
   time_t interval;
   float tolerance;
};

struct Config {
   SensorConfig sensors[4];
   float temp_max;
   float temp_min;
   float hum_max;
   float hum_min;
   byte fans_power_pin;
   byte fans_conmutation_pin;
   byte ac_pins[2];
};
*/

#endif
