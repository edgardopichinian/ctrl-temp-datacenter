#ifndef _TEMPERATURE_SUBJECT_H
#define _TEMPERATURE_SUBJECT_H

#include "tolerance_filter.h"

class TemperatureSubject : public ToleranceFilter {
	public:
		TemperatureSubject(DhtSensor* s, float t) : ToleranceFilter(s, t) { 
			Serial.println("TemperatureSubject created");
		}
		
	protected:
		float extractValue() const;
};

#endif
