#include "subject.h"


void Subject::subscribe(Observer* obs)  {
	if(lastObserver < MAX_OBSERVERS) {
		lastObserver++;
		observers[lastObserver] = obs;
		Serial.print("observer added ");
		Serial.println(lastObserver);
	}
}

void Subject::unsubscribe(Observer* obs) {
	for(char i=0; i<=lastObserver; i++) {
		if(observers[i] == obs) {
			observers[i] = observers[lastObserver];
			lastObserver--;
			Serial.println("observer removed");
			return;
		}
	}
}

void Subject::notifyObservers(const Subject* source) {
//	Serial.print("Subject.notifyObservers() ");
//	Serial.println(lastObserver);
	for(char i=0; i<=lastObserver; i++) {
		observers[i]->notify(source);
	}
}
