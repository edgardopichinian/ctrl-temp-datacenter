#ifndef _MQTT_DATA_RECEIVER_H
#define _MQTT_DATA_RECEIVER_H

#include <PubSubClient.h>
// #include <ArduinoJson.h>
#include "subject.h"

class MqttDataReceiver : public Subject {
	public:
		void receive(char* topic, byte* payload, unsigned int length);
		char* getTopic() { return topic; }
		String getPayload() { return payload; }
		unsigned int getLength() { return length; }

	private:
		char* topic;
		String payload;
		unsigned int length;
};

#endif
