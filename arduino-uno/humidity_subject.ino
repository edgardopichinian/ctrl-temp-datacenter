#include "humidity_subject.h"

float HumiditySubject::extractValue() const {
//	Serial.println("HumiditySubject.extractValue()");
	return getSensor()->getHumidity();
}
