#ifndef _SUBJECT_H
#define _SUBJECT_H

#define MAX_OBSERVERS 8

#include "observer.h"

class Subject {
	public:
		virtual ~Subject() {}
		void subscribe(Observer* obs);
		void unsubscribe(Observer* obs);

	protected:
		virtual void notifyObservers(const Subject* source);

	private:
		Observer* observers[MAX_OBSERVERS];
		char lastObserver = -1;
};

#endif
